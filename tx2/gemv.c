/* POLYBENCH/GPU-OPENACC
 *
 * This file is a part of the Polybench/GPU-OpenACC suite
 *
 * Contact:
 * William Killian <killian@udel.edu>
 * 
 * Copyright 2013, The University of Delaware
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

/* Include polybench common header. */
#include "polybench.h"

/* Include benchmark-specific header. */
/* Default data type is double, default size is 4000. */
#include "gemm.h"

#include <time.h>
#define CLOCK_PRECISION 1E9
double accum;


/* Array initialization. */
static void init_array(int n,
		DATA_TYPE *alpha,
		DATA_TYPE *beta,
		DATA_TYPE POLYBENCH_1D(y,N,n),
		DATA_TYPE POLYBENCH_2D(A,N,N,n,n),
		DATA_TYPE POLYBENCH_1D(x,N,n))
{
  int i, j;

  *alpha = 32412;
  *beta = 2123;
  for (i = 0; i < N; i++){
		x[i] = ((DATA_TYPE) i) / N;
		y[i] = ((DATA_TYPE) i) / N;
    for (j = 0; j < N; j++)
      A[i][j] = ((DATA_TYPE) i*j) / N;
	}
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static
void print_array(int n,
		 DATA_TYPE POLYBENCH_1D(y,N,n))
{
  int i;

  for (i = 0; i < N; i++){
		fprintf (stderr, DATA_PRINTF_MODIFIER, y[i]);
		if ((i * n) % 20 == 0) fprintf (stderr, "\n");
    }
  fprintf (stderr, "\n");
}


/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static
void kernel_gemv(int n,
		 DATA_TYPE alpha,
		 DATA_TYPE beta,
		 DATA_TYPE POLYBENCH_1D(y,N,n),
		 DATA_TYPE POLYBENCH_2D(A,N,N,n,n),
		 DATA_TYPE POLYBENCH_1D(x,N,n))
{
  int i, j;
 	#pragma omp target data map(tofrom: y[0:N]) map(to: A[0:N], x[0:N])
#pragma omp target teams distribute parallel for schedule(static, 1) \
	num_teams(NUM_TEAM) \
	num_threads(NUM_THREAD)
	for (j = 0; j < N; j++) {
		y[j] *= beta;
		for (i = 0; i < N; i++) {
			y[j] += alpha * A[j][i] * x[i];
		}
	}
}

typedef struct {
	time_t tv_sec;
	long tv_nsec;
} my_timespec;

int main(int argc, char** argv)
{
#ifdef TIMEKERN
	my_timespec KernStrt, KernStop;
#endif
#ifdef TIMEPROG
	my_timespec ProgStrt, ProgStop;
	clock_gettime(CLOCK_MONOTONIC, &ProgStrt);
#endif

  /* Retrieve problem size. */
  int n = N;

  /* Variable declaration/allocation. */
  DATA_TYPE alpha;
  DATA_TYPE beta;
  POLYBENCH_1D_ARRAY_DECL(y,DATA_TYPE,N,n);
  POLYBENCH_2D_ARRAY_DECL(A,DATA_TYPE,N,N,n,n);
  POLYBENCH_1D_ARRAY_DECL(x,DATA_TYPE,N,n);

  /* Initialize array(s). */
  init_array (n, &alpha, &beta,
	      POLYBENCH_ARRAY(x),
	      POLYBENCH_ARRAY(A),
	      POLYBENCH_ARRAY(y));

#ifdef TIMEKERN
	clock_gettime(CLOCK_MONOTONIC, &KernStrt);
#endif 

  /* Run kernel. */
  kernel_gemv (n,
	       alpha, beta,
	       POLYBENCH_ARRAY(y),
	       POLYBENCH_ARRAY(A),
	       POLYBENCH_ARRAY(x));

#ifdef TIMEKERN
	clock_gettime(CLOCK_MONOTONIC, &KernStop);
#endif 

  /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
  polybench_prevent_dce(print_array(n, POLYBENCH_ARRAY(y)));

  /* Be clean. */
  POLYBENCH_FREE_ARRAY(y);
  POLYBENCH_FREE_ARRAY(A);
  POLYBENCH_FREE_ARRAY(x);

#ifdef TIMEPROG
	clock_gettime(CLOCK_MONOTONIC, &ProgStop);
	accum = (ProgStop.tv_sec-ProgStrt.tv_sec)
		+(ProgStop.tv_nsec-ProgStrt.tv_nsec)/CLOCK_PRECISION;
	printf("PROGTIME: %lf,", accum);
#endif

#ifdef TIMEKERN
	accum = (KernStop.tv_sec-KernStrt.tv_sec)
		+(KernStop.tv_nsec-KernStrt.tv_nsec)/CLOCK_PRECISION;
	printf("KERNTIME: %lf,", accum);
#endif

  return 0;
}
