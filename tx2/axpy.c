/* POLYBENCH/GPU-OPENACC
 *
 * This file is a part of the Polybench/GPU-OpenACC suite
 *
 * Contact:
 * William Killian <killian@udel.edu>
 *
 * Copyright 2013, The University of Delaware
 */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* Include polybench common header. */
#include "polybench.h"

/* Include benchmark-specific header. */
/* Default data type is double, default size is 4000. */
#include "gemm.h"

#include <time.h>
#define CLOCK_PRECISION 1E9
double accum;

/* Array initialization. */
static void init_array(int n, DATA_TYPE *alpha, DATA_TYPE POLYBENCH_1D(Y, N, n),
                       DATA_TYPE POLYBENCH_1D(X, N, n)) {
  int i, j;

  *alpha = 32412;
  for (i = 0; i < N; i++)
    Y[i] = (DATA_TYPE)i / N;
}

/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static void print_array(int n, DATA_TYPE POLYBENCH_1D(Y, N, n)) {
  int i;

  for (i = 0; i < N; i++) {
    fprintf(stderr, DATA_PRINTF_MODIFIER, Y[i]);
  }
  fprintf(stderr, "\n");
}

/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static void kernel_axpy(int n, DATA_TYPE alpha, DATA_TYPE POLYBENCH_1D(Y, N, n),
                        DATA_TYPE POLYBENCH_1D(X, N, n)) {
  int i;
#pragma omp target data map(tofrom : Y[0 : N]) map(to : X[0 : N])
#pragma omp target teams distribute parallel for schedule(                     \
    static, 1) num_teams(NUM_TEAM) num_threads(NUM_THREAD)
  for (i = 0; i < N; i++) {
    Y[i] += alpha * X[i];
  }
}

typedef struct {
  time_t tv_sec;
  long tv_nsec;
} my_timespec;

int main(int argc, char **argv) {
#ifdef TIMEKERN
  my_timespec KernStrt, KernStop;
#endif
#ifdef TIMEPROG
  my_timespec ProgStrt, ProgStop;
  clock_gettime(CLOCK_MONOTONIC, &ProgStrt);
#endif

  /* Retrieve problem size. */
  int n = N;

  /* Variable declaration/allocation. */
  DATA_TYPE alpha;
  DATA_TYPE beta;
  POLYBENCH_1D_ARRAY_DECL(Y, DATA_TYPE, N, n);
  POLYBENCH_1D_ARRAY_DECL(X, DATA_TYPE, N, n);

  /* Initialize array(s). */
  init_array(n, &alpha, POLYBENCH_ARRAY(Y), POLYBENCH_ARRAY(X));

#ifdef TIMEKERN
  clock_gettime(CLOCK_MONOTONIC, &KernStrt);
#endif

  /* Run kernel. */
  kernel_axpy(n, alpha, POLYBENCH_ARRAY(Y), POLYBENCH_ARRAY(X));

#ifdef TIMEKERN
  clock_gettime(CLOCK_MONOTONIC, &KernStop);
#endif

  /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
  polybench_prevent_dce(print_array(n, POLYBENCH_ARRAY(Y)));

  /* Be clean. */
  POLYBENCH_FREE_ARRAY(Y);
  POLYBENCH_FREE_ARRAY(X);

#ifdef TIMEPROG
  clock_gettime(CLOCK_MONOTONIC, &ProgStop);
  accum = (ProgStop.tv_sec - ProgStrt.tv_sec) +
          (ProgStop.tv_nsec - ProgStrt.tv_nsec) / CLOCK_PRECISION;
  printf("PROGTIME: %lf,", accum);
#endif

#ifdef TIMEKERN
  accum = (KernStop.tv_sec - KernStrt.tv_sec) +
          (KernStop.tv_nsec - KernStrt.tv_nsec) / CLOCK_PRECISION;
  printf("KERNTIME: %lf,", accum);
#endif

  return 0;
}
