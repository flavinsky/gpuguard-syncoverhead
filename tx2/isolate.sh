#!/bin/bash

TRACE_PATH=/sys/kernel/debug/tracing

echo | sudo tee  $TRACE_PATH/trace > /dev/null
echo $1 | sudo tee $TRACE_PATH/events/sched/enable > /dev/null
sudo sh -c "echo $1 | tee $TRACE_PATH/events/timer/hrtimer_*/enable > /dev/null"
echo $1 | sudo tee $TRACE_PATH/events/gk20a/gk20a_push_cmdbuf/enable > /dev/null
echo $1 | sudo tee $TRACE_PATH/events/irq/enable > /dev/null
echo $1 | sudo tee $TRACE_PATH/events/signal/enable > /dev/null
echo $1 | sudo tee $TRACE_PATH/events/printk/enable > /dev/null
#echo $1 | sudo tee $TRACE_PATH/events/syscalls/enable > /dev/null

