#!/bin/bash

PATH_YKT_ROOT=/home/kreilfla/nfs/prem/benchmark/ibm/clang-ykt
OLD_PATH=$PATH
OLD_LD_LIBRARY_PATH=$LD_LIBRARY_PATH
OLD_LIBRARY_PATH=$LIBRARY_PATH
ITER=1

format_unit (){
	case $2 in
	"ns")
	  new_value=$1'e-9'
	  ;;
	"us")
	  new_value=$1'e-6'
	  ;;
	"ms")
	  new_value=$1'e-3'
	  ;;
	*)
	  new_value=$1
	  ;;
	esac	
	echo $new_value
}

run_hercules_nohv(){
	# Export Hercules paths
	export PATH=$OLD_PATH:$HOME/opt/hercules-compiler/bin
	export LIBRARY_PATH=$OLD_LIBRARY_PATH:$HOME/opt/hercules-compiler/lib
	export LD_LIBRARY_PATH=$OLD_LD_LIBRARY_PATH:$HOME/opt/hercules-compiler/lib

	echo "nohv_kernel_time, nohv_total_time, nohv_setup, nohv_exit, nohv_tgt_rtl_data_alloc, nohv_tgt_rtl_data_delete, nohv_tgt_rtl_data_retrieve, nohv_tgt_rtl_data_submit, nohv_tgt_rtl_init_device, nohv_tgt_rtl_load_binary, nohv_tgt_rtl_run_target_team_region"

	for (( c=1; c<=$ITER; c++ )) do
		output=$( ( taskset 0x01 nvprof ./$1-$2-hercules-bin) 2>&1)
		#result=$(( $your_command ) 2>&1)
		data_alloc=$(grep -Po 'Range "tgt_rtl_data_alloc" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_alloc" \(\d+ times, total time: \d+\.\d+\K[^\)]+' <<< $output)
		data_alloc=$(format_unit $data_alloc $unit)

		data_delete=$(grep -Po 'Range "tgt_rtl_data_delete" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_delete" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		data_delete=$(format_unit $data_delete $unit)

		data_retrieve=$(grep -Po 'Range "tgt_rtl_data_retrieve" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_retrieve" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		data_retrieve=$(format_unit $data_retrieve $unit)
		
		data_submit=$(grep -Po 'Range "tgt_rtl_data_submit" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_submit" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		data_submit=$(format_unit $data_submit $unit)
		
		init_device=$(grep -Po 'Range "tgt_rtl_init_device" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_init_device" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		init_device=$(format_unit $init_device $unit)
		
		load_binary=$(grep -Po 'Range "tgt_rtl_load_binary" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_load_binary" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		load_binary=$(format_unit $load_binary $unit)
		
		run_team=$(grep -Po 'Range "tgt_rtl_run_target_team_region" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_run_target_team_region" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		run_team=$(format_unit $run_team $unit)

		time_setup=$(grep -Po 'tSe: \K[^s]+' <<< $output)
		time_exit=$(grep -Po 'tEx: \K[^s]+' <<< $output)
		kern_time=$(grep -Po 'KERNTIME: \K[^,]+' <<< $output)
		prog_time=$(grep -Po 'PROGTIME: \K[^,]+' <<< $output)
		
#		echo "$output"
		echo $kern_time, $prog_time, $time_setup, $time_exit, $data_alloc, $data_delete, $data_retrieve, $data_submit, $init_device, $load_binary, $run_team
	done
}

run_hercules_hv(){
	# Export Hercules paths
	export PATH=$OLD_PATH:$HOME/opt/hercules-compiler/bin
	export LIBRARY_PATH=$OLD_LIBRARY_PATH:$HOME/opt/hercules-compiler/lib
	export LD_LIBRARY_PATH=$OLD_LD_LIBRARY_PATH:$HOME/opt/hercules-compiler/lib

	echo "hv_kernel_time, hv_total_time, hv_setup, hv_exit, hv_tgt_rtl_data_alloc, hv_tgt_rtl_data_delete, hv_tgt_rtl_data_retrieve, hv_tgt_rtl_data_submit, hv_tgt_rtl_init_device, hv_tgt_rtl_load_binary, hv_tgt_rtl_run_target_team_region"

	for (( c=1; c<=$ITER; c++ )) do
		output=$( ( taskset 0x01 nvprof ./$1-$2-hercules-bin) 2>&1)
		
		data_alloc=$(grep -Po 'Range "tgt_rtl_data_alloc" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_alloc" \(\d+ times, total time: \d+\.\d+\K[^\)]+' <<< $output)
		data_alloc=$(format_unit $data_alloc $unit)

		data_delete=$(grep -Po 'Range "tgt_rtl_data_delete" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_delete" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		data_delete=$(format_unit $data_delete $unit)

		data_retrieve=$(grep -Po 'Range "tgt_rtl_data_retrieve" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_retrieve" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		data_retrieve=$(format_unit $data_retrieve $unit)
		
		data_submit=$(grep -Po 'Range "tgt_rtl_data_submit" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_submit" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		data_submit=$(format_unit $data_submit $unit)
		
		init_device=$(grep -Po 'Range "tgt_rtl_init_device" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_init_device" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		init_device=$(format_unit $init_device $unit)
		
		load_binary=$(grep -Po 'Range "tgt_rtl_load_binary" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_load_binary" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		load_binary=$(format_unit $load_binary $unit)
		
		run_team=$(grep -Po 'Range "tgt_rtl_run_target_team_region" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_run_target_team_region" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		run_team=$(format_unit $run_team $unit)
		
		time_setup=$(grep -Po 'tSe: \K[^s]+' <<< $output)
		time_exit=$(grep -Po 'tEx: \K[^s]+' <<< $output)
		kern_time=$(grep -Po 'KERNTIME: \K[^,]+' <<< $output)
		prog_time=$(grep -Po 'PROGTIME: \K[^,]+' <<< $output)
		
		#echo "$output"
		echo $kern_time, $prog_time, $time_setup, $time_exit, $data_alloc, $data_delete, $data_retrieve, $data_submit, $init_device, $load_binary, $run_team
	done
}

run_noprem(){
	export PATH=$OLD_PATH:$HOME/opt/hercules-compiler/bin
	export LIBRARY_PATH=$OLD_LIBRARY_PATH:$HOME/opt/hercules-compiler/lib
	export LD_LIBRARY_PATH=$OLD_LD_LIBRARY_PATH:$HOME/opt/hercules-compiler/lib
	echo "noprem_kernel_time, noprem_total_time, noprem_tgt_rtl_data_alloc, noprem_tgt_rtl_data_delete, noprem_tgt_rtl_data_retrieve, noprem_tgt_rtl_data_submit, noprem_tgt_rtl_init_device, noprem_tgt_rtl_load_binary, noprem_tgt_rtl_run_target_team_region"
	for (( c=1; c<=$ITER; c++ )) do
		output=$( ( taskset 0x01 nvprof ./$1-$2-noprem-bin) 2>&1)
		
		data_alloc=$(grep -Po 'Range "tgt_rtl_data_alloc" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_alloc" \(\d+ times, total time: \d+\.\d+\K[^\)]+' <<< $output)
		data_alloc=$(format_unit $data_alloc $unit)

		data_delete=$(grep -Po 'Range "tgt_rtl_data_delete" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_delete" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		data_delete=$(format_unit $data_delete $unit)

		data_retrieve=$(grep -Po 'Range "tgt_rtl_data_retrieve" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_retrieve" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		data_retrieve=$(format_unit $data_retrieve $unit)
		
		data_submit=$(grep -Po 'Range "tgt_rtl_data_submit" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_data_submit" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		data_submit=$(format_unit $data_submit $unit)
		
		init_device=$(grep -Po 'Range "tgt_rtl_init_device" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_init_device" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		init_device=$(format_unit $init_device $unit)
		
		load_binary=$(grep -Po 'Range "tgt_rtl_load_binary" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_load_binary" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		load_binary=$(format_unit $load_binary $unit)
		
		run_team=$(grep -Po 'Range "tgt_rtl_run_target_team_region" \(\d+ times, total time: \K[^\p{L}]+' <<< $output)
		unit=$(grep -Po 'Range "tgt_rtl_run_target_team_region" \(\d+ times, total time: \d+.\d+\K[^\)]+' <<< $output)
		run_team=$(format_unit $run_team $unit)

		kern_time=$(grep -Po 'KERNTIME: \K[^,]+' <<< $output)
		prog_time=$(grep -Po 'PROGTIME: \K[^,]+' <<< $output)
		echo $kern_time, $prog_time, $data_alloc, $data_delete, $data_retrieve, $data_submit, $init_device, $load_binary, $run_team
	done
}

train_hv(){
	# Export Hercules paths
	export PATH=$OLD_PATH:$HOME/opt/hercules-compiler/bin
	export LIBRARY_PATH=$OLD_LIBRARY_PATH:$HOME/opt/hercules-compiler/lib
	export LD_LIBRARY_PATH=$OLD_LD_LIBRARY_PATH:$HOME/opt/hercules-compiler/lib

	for (( c=1; c<=$ITER; c++ )) do
		taskset 0x01 nvprof ./$1-$2-hercules-bin
		echo "========================================================================"
		echo "Iteration" $c
		echo "========================================================================"
		sleep 0.1
	done

}

sudo insmod gg-lkm/gguard.hv.ko
#sudo rmmod gguard
#sudo insmod gg-lkm/gguard.nohv.ko
for (( SETSIZE=701; SETSIZE<=701; SETSIZE=SETSIZE+100 )) do
	
#	make clean 2>&1
#	ln -s -f -T hercules-compiler-gg-scratch /home/kreilfla/opt/hercules-compiler
#	make hercules DATA_SIZE=$SETSIZE BENCH_TRG=$1 >/dev/null 2>&1
#	
#	sudo rmmod gguard
#	sudo insmod gg-lkm/gguard.hv.ko
#	run_hercules_hv $1 $SETSIZE| tee -a tmp.herculeshv
#
#	ln -s -f -T hercules-compiler-nogg-scratch /home/kreilfla/opt/hercules-compiler
#	make noprem DATA_SIZE=$SETSIZE BENCH_TRG=$1 >/dev/null 2>&1
#       
#	sudo rmmod gguard
#	sudo insmod gg-lkm/gguard.nohv.ko
#	run_noprem $1 $SETSIZE| tee -a tmp.noprem
#
#	paste tmp.noprem tmp.herculeshv | column -s $',' -t > $SETSIZE$1.csv
#	rm tmp.*
	
	make clean 2>&1
	ln -s -f -T hercules-compiler-gg-scratch /home/kreilfla/opt/hercules-compiler
	make hercules DATA_SIZE=$SETSIZE BENCH_TRG=$1 >/dev/null 2>&1
	train_hv $1 $SETSIZE
	
done
sudo rmmod gguard
make clean
